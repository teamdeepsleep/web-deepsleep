import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IHabito } from '../model/IHabito';
import { IRespuestaDTO } from '../model/IDto';

const base_url = environment.base_url;

@Injectable({
	providedIn: 'root',
})
export class HabitoService {
	constructor(private http: HttpClient, private router: Router) {}

	cargarHabitos() {
		const url = `${base_url}/habito`;
		return this.http.get<IRespuestaDTO<IHabito[]>>(url);
	}

	buscarHabitoPorId(id: string) {
		const url = `${base_url}/habito/${id}`;
		console.log(url);
		return this.http.get<IRespuestaDTO<IHabito>>(url);
	}

	crearHabito(habito: any) {
		const url = `${base_url}/habito`;
		return this.http.post<IRespuestaDTO<IHabito>>(url, habito);
	}

	actualizarHabito(habito: any, id: string) {
		const url = `${base_url}/habito/${id}`;

		return this.http.put(url, habito);
	}

	eliminarHabito(id: string) {
		const url = `${base_url}/habito/${id}`;

		return this.http.delete(url);
	}

	async actualizarFoto(archivo: File, id: string) {
		try {
			// C:\allData\repos\REPO_DEEPSLEEP\backend-deepsleep\src\app\uploads\recomendaciones\0b0cd28b-4500-4599-809f-226ec9e3d2b1.jpg
			// const url = `C:/allData/repos/REPO_DEEPSLEEP/backend-deepsleep/src/app/uploads/recomendaciones/c53b44d1-ce47-4806-9992-bb495924d9f3.jpg`
			// const url = `C:/allData/repos/REPO_DEEPSLEEP/backend-deepsleep/src/app/uploads/recomendaciones/c53b44d1-ce47-4806-9992-bb495924d9f3.jpg`;
			const url = `${base_url}/upload/habito/${id}`;
			const formData = new FormData();
			formData.append('imagen', archivo);

			const resp = await fetch(url, {
				method: 'PUT',
				body: formData,
			});

			const data = await resp.json();
			data.nombreArchivo = `${base_url}/upload/habito/${id}`;
			console.log(data);

			return data;
		} catch (error) {
			return {
				operacion: false,
				menssaje: 'error en actualizarFoto',
			};
		}
	}
}
