import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IEspecialista } from '../model/IEspecialista';

const base_url = environment.base_url;

@Injectable({
	providedIn: 'root',
})
export class EspecialistaService {
	constructor(private http: HttpClient, private router: Router) {}

	cargarEspecialistas() {
		const url = `${base_url}/especialista`;

		return this.http.get<IEspecialista[]>(url);
	}

	cambiarEstadoEspecialista(id: string, estado: boolean) {
		const url = `${base_url}/especialista/estado`;

		return this.http.put(url, {
			id,
			estado,
		});
	}
}
