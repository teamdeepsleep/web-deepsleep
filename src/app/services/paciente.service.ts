import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IPaciente } from '../model/IPaciente';

const base_url = environment.base_url;

@Injectable({
	providedIn: 'root',
})
export class PacienteService {
	constructor(private http: HttpClient, private router: Router) {}

	cargarPacientes() {
		const url = `${base_url}/paciente`;

		return this.http.get<IPaciente[]>(url);
	}

	cambiarEstadoPaciente(id: string, estado: boolean) {
		const url = `${base_url}/paciente/estado`;

		return this.http.put(url, {
			id,
			estado,
		});
	}
}
