export interface IRespuestaDTO<T> {
	operacion: boolean;
	data: T | null;
	mensaje: string;
}
