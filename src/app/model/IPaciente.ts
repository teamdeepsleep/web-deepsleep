export interface IPaciente {
	_id: string;
	nombre: string;
	apellido: string;
	correo: string;
	habilitado: boolean;
	dni: string;
	imagen?: string;
	psqi: number;
}
