export interface IRecomendacion {
	_id: string;
	titulo: string;
	descripcion: string;
	imagen?: string;
	habitos: string[];
}
