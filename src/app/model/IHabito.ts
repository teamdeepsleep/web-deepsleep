export interface IHabito {
	_id: string;
	nombre: string;
	imagen?: string;
}
