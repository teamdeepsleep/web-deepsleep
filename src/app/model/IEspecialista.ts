export interface IEspecialista {
	_id: string;
	nombre: string;
	apellido: string;
	correo: string;
	imagen?: string;
	habilitado: boolean;
	dni: string;
}
