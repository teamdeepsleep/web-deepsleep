import {
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
	NO_ERRORS_SCHEMA,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NopagefoundComponent } from './pages/nopagefound/nopagefound.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HeaderComponent } from './shared/header/header.component';
import { PacientesComponent } from './pages/mantenimiento/pacientes/pacientes.component';
import { EspecialistasComponent } from './pages/mantenimiento/especialistas/especialistas.component';
import { RecomendacionesComponent } from './pages/mantenimiento/recomendaciones/recomendaciones.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { RecomendacionComponent } from './pages/mantenimiento/recomendaciones/recomendacion/recomendacion.component';
import { HabitosComponent } from './pages/mantenimiento/habitos/habitos.component';
import { HabitoComponent } from './pages/mantenimiento/habitos/habito/habito.component';
import { ImagenPipe } from './pipes/imagen.pipe';

@NgModule({
	declarations: [
		AppComponent,
		NopagefoundComponent,
		DashboardComponent,
		SidebarComponent,
		HeaderComponent,
		PacientesComponent,
		EspecialistasComponent,
		RecomendacionesComponent,
		RecomendacionComponent,
  HabitosComponent,
  HabitoComponent,
  ImagenPipe,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
	],
	providers: [],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}
