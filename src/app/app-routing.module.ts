import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NopagefoundComponent } from './pages/nopagefound/nopagefound.component';
import { PacientesComponent } from './pages/mantenimiento/pacientes/pacientes.component';
import { EspecialistasComponent } from './pages/mantenimiento/especialistas/especialistas.component';
import { RecomendacionesComponent } from './pages/mantenimiento/recomendaciones/recomendaciones.component';
import { RecomendacionComponent } from './pages/mantenimiento/recomendaciones/recomendacion/recomendacion.component';
import { HabitosComponent } from './pages/mantenimiento/habitos/habitos.component';
import { HabitoComponent } from './pages/mantenimiento/habitos/habito/habito.component';

const routes: Routes = [
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'pacientes', component: PacientesComponent },
	{ path: 'especialistas', component: EspecialistasComponent },
	{ path: 'habitos', component: HabitosComponent },
	// { path: 'habito', component: HabitoComponent },
	{
		path: 'habito-nuevo',
		component: HabitoComponent,
		data: { titulo: 'Habito Aplicación' },
	},
	{
		path: 'habito-editar/:id',
		component: HabitoComponent,
		data: { titulo: 'Habito Aplicación' },
	},
	{ path: 'recomendaciones', component: RecomendacionesComponent },
	// { path: 'recomendacion', component: RecomendacionesComponent },
	{
		path: 'recomendacion-nuevo',
		component: RecomendacionComponent,
		data: { titulo: 'Recomendacion Aplicación' },
	},
	{
		path: 'recomendacion-editar/:id',
		component: RecomendacionComponent,
		data: { titulo: 'Recomendacion Aplicación' },
	},
	{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
	{ path: '**', component: NopagefoundComponent },
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
