import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IHabito } from 'src/app/model/IHabito';
import { HabitoService } from 'src/app/services/habito.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-habitos',
	templateUrl: './habitos.component.html',
	styles: [],
})
export class HabitosComponent implements OnInit {
	public cargando: boolean = true;
	public habitos: IHabito[] = [];

	constructor(private router: Router, private habitoService: HabitoService) {}

	ngOnInit(): void {
		this.cargarHabitos();
	}

	cargarHabitos() {
		this.cargando = true;
		this.habitoService.cargarHabitos().subscribe((data) => {
			this.habitos = data.data || [];
			this.cargando = false;
		});
	}

	irCrearHabito() {
		this.router.navigate(['/habito-nuevo/']);
	}
	irEditarHabito(id: any) {
		this.router.navigate(['/habito-editar/' + id]);
	}
	eliminarHabito(id: any) {
		Swal.fire({
			title: '¿Eliminar Hábito?',
			text: `Esta a punto de borrar el hábito`,
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si Borrarlo',
		}).then((result) => {
			if (result.value) {
				this.habitoService.eliminarHabito(id).subscribe((resp: any) => {
					if (resp.operacion === true) {
						this.cargarHabitos();
						Swal.fire('Hábito eliminado', resp.mensaje, 'success');
					}
				});
			}
		});
	}

	verImagen(habito: IHabito) {
		// var base_url = environment.base_url;
		return habito.imagen;
		// return `${base_url}/upload/imagen/habitos/${habito.imagen}`;
	}
}
