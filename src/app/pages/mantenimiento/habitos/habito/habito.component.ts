import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { HabitoService } from 'src/app/services/habito.service';
import { IHabito } from 'src/app/model/IHabito';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-habito',
	templateUrl: './habito.component.html',
	styles: [],
})
export class HabitoComponent implements OnInit {
	public id: string = '';
	public imagenTemporal: string | null = null;
	public imagenDB: any;
	public imagenHabito: string = '';
	public imagenSubir!: File;
	public formularioCrearHabito = this.fb.group({
		nombre: ['', [Validators.required]],
	});

	constructor(
		private fb: FormBuilder,
		private router: Router,
		private route: ActivatedRoute,
		private habitoService: HabitoService,
	) {}

	ngOnInit(): void {
		this.id = this.route.snapshot.paramMap.get('id') || '';
		if (this.id) {
			console.log('id encontrado...');
			this.buscarHabitoPorId();
		} else {
			console.log('id no encontrado');
		}
	}

	OperacionDeCrearActualizar() {
		if (this.id === '') {
			this.crearHabito();
		} else {
			this.actualizarHabito();
		}
	}

	buscarHabitoPorId() {
		this.habitoService.buscarHabitoPorId(this.id).subscribe((resp) => {
			// const base_url = environment.base_url;
			var habito = resp.data;
			this.imagenDB = habito!.imagen;
			// this.imagenLocal = `${base_url}/upload/imagen/recomendaciones/${
			// 	habito!.imagen
			// }`;
			this.formularioCrearHabito.get('nombre')?.setValue(habito?.nombre);
		});
	}
	crearHabito() {
		if (this.formularioCrearHabito.invalid) {
			Swal.fire({
				icon: 'warning',
				title: 'Oops...',
				text: 'Se requiere completar todos los datos para continuar',
			});
			return;
		}

		var newHabito = {
			nombre: this.formularioCrearHabito.value.nombre,
		};

		this.habitoService.crearHabito(newHabito).subscribe((resp: any) => {
			if (resp.operacion) {
				Swal.fire('Hábito creado', resp.mensaje, 'success');
				this.irHabitos();
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: resp.mensaje,
				});
			}
		});
	}
	actualizarHabito() {
		var updateHabito = {
			nombre: this.formularioCrearHabito.value.nombre,
		};

		this.habitoService
			.actualizarHabito(updateHabito, this.id)
			.subscribe((resp: any) => {
				if (resp.operacion) {
					Swal.fire('Hábito actualizado', resp.mensaje, 'success');
					this.irHabitos();
				} else {
				}
			});
	}

	cambiarImagen(event: Event) {
		console.log(event);
		const target = event.target as HTMLInputElement;
		const archivo: File = (target.files as FileList)[0];

		this.imagenSubir = archivo;
		if (!archivo) {
			console.log('sin carga archivo', this.imagenTemporal);
			return (this.imagenTemporal = null);
		}
		const reader = new FileReader();
		reader.readAsDataURL(archivo);
		reader.onload = () => {
			console.log('archivo', reader.result);
			this.imagenTemporal = reader.result as string;
		};

		return null;
	}

	subirImagen() {
		this.habitoService
			.actualizarFoto(this.imagenSubir, this.id)
			.then((resp: any) => {
				if (resp.operacion) {
					Swal.fire(
						'Actualización del hábito',
						// 'Imagen de médico actualizado correctamente',
						resp.mensaje,
						'success',
					);

					// this.imagenRecomendacion = `C:/allData/repos/REPO_DEEPSLEEP/backend-deepsleep/src/app/uploads/recomendaciones/${resp.nombreArchivo}.jpg`;
				} else {
					Swal.fire(
						'Actualización del hábito',
						// 'Imagen de médico actualizado correctamente',
						resp.mensaje,
						'error',
					);
				}
			})
			.catch((err) => {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: '¡Algo salió mal!',
				});
			});
	}
	irHabitos() {
		this.router.navigate(['/habitos']);
	}

	validarCreacionoUpdate() {
		if (this.id == '') {
			return 'https://us.123rf.com/450wm/urfandadashov/urfandadashov1809/urfandadashov180901275/109135379-photo-not-available-vector-icon-isolated-on-transparent-background-photo-not-available-logo-concept.jpg?ver=6';
		}
		return this.imagenDB;
	}
}
