import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { RecomendacionService } from 'src/app/services/recomendacion.service';
import Swal from 'sweetalert2';
import { IRecomendacion } from 'src/app/model/IRecomendacion';
import { HabitoService } from 'src/app/services/habito.service';
import { IHabito } from 'src/app/model/IHabito';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-recomendacion',
	templateUrl: './recomendacion.component.html',
	styles: [],
})
export class RecomendacionComponent implements OnInit {
	public id: string = '';
	// public habitos: IHabito[] = [];
	public seleccionHabitos: IHabitoSelect[] = [];
	public imagenTemporal: string | null = null;
	public imagenDB: any;
	public imagenRecomendacion: string = '';
	public imagenSubir!: File;
	public formularioCrearRecomendacion = this.fb.group({
		titulo: ['', [Validators.required]],
		descripcion: ['', [Validators.required]],
	});
	public xd: any = 4;
	constructor(
		private fb: FormBuilder,
		private router: Router,
		private route: ActivatedRoute,
		private recomendacionService: RecomendacionService,
		private habitoService: HabitoService,
	) {}

	ngOnInit(): void {
		this.id = this.route.snapshot.paramMap.get('id') || '';
		if (this.id) {
			console.log('id encontrado...');
			// this.imagenTemporal = null;
			this.buscarRecomendacionPorId();
		} else {
			console.log('id no encontrado');
			this.cargarTodosHabitos();
		}
	}

	cargarTodosHabitos() {
		this.habitoService.cargarHabitos().subscribe((data) => {
			data.data?.map((habito) => {
				this.seleccionHabitos.push({
					_id: habito._id,
					nombre: habito.nombre,
					isSelected: false,
				});
			});
		});
	}

	cargarMisHabitos(habitos: string[]) {
		this.habitoService.cargarHabitos().subscribe((data) => {
			data.data?.map((habito) => {
				this.seleccionHabitos.push({
					_id: habito._id,
					nombre: habito.nombre,
					isSelected: false,
				});
			});

			habitos.map((idHabito) => {
				const x = this.seleccionHabitos.find((h) => h._id === idHabito);
				if (x) {
					x.isSelected = true;
				}
			});
		});
	}

	OperacionDeCrearActualizar() {
		if (this.id === '') {
			this.crearRecomendacion();
		} else {
			this.actualizarRecomendacion();
		}
	}

	crearRecomendacion() {
		if (this.formularioCrearRecomendacion.invalid) {
			Swal.fire({
				icon: 'warning',
				title: 'Oops...',
				text: 'Se requiere completar todos los datos para continuar',
			});
			return;
		}
		var x: string[] = [];
		var newRecomendacion = {
			_id: '',
			titulo: this.formularioCrearRecomendacion.value.titulo,
			descripcion: this.formularioCrearRecomendacion.value.descripcion,
			habitos: x,
		};

		this.seleccionHabitos.map((h) => {
			if (h.isSelected === true) {
				newRecomendacion.habitos.push(h._id);
			}
		});

		this.recomendacionService
			.crearRecomendacion(newRecomendacion)
			.subscribe((resp: any) => {
				if (resp.operacion) {
					Swal.fire('Recomendación creada', resp.mensaje, 'success');
					this.irRecomendaciones();
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: resp.mensaje,
					});
				}
			});
	}
	actualizarRecomendacion() {
		var x: string[] = [];
		var updateRecomendacion = {
			titulo: this.formularioCrearRecomendacion.value.titulo,
			descripcion: this.formularioCrearRecomendacion.value.descripcion,
			habitos: x,
		};

		this.seleccionHabitos.map((h) => {
			if (h.isSelected === true) {
				updateRecomendacion.habitos.push(h._id);
			}
		});

		this.recomendacionService
			.actualizarRecomendacion(updateRecomendacion, this.id)
			.subscribe((resp: any) => {
				if (resp.operacion) {
					Swal.fire(
						'Recomendación actualizada',
						resp.mensaje,
						'success',
					);
					this.irRecomendaciones();
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: resp.mensaje,
					});
				}
			});
	}
	buscarRecomendacionPorId() {
		this.recomendacionService
			.buscarRecomendacionPorId(this.id)
			.subscribe((resp) => {
				this.imagenDB = resp.imagen;
				this.formularioCrearRecomendacion
					.get('titulo')
					?.setValue(resp.titulo);

				this.formularioCrearRecomendacion
					.get('descripcion')
					?.setValue(resp.descripcion);

				this.cargarMisHabitos(resp.habitos);
			});
	}

	seleccionDeHabitos(event: Event, habito: any) {
		const target = event.target as HTMLInputElement;

		habito.isSelected = target.checked;
		// console.log(habito);
		// var x = this.seleccionHabitos.find((h) => h._id === habito._id);
		// console.log(this.seleccionHabitos);
	}

	cambiarImagen(event: Event) {
		console.log(event);
		const target = event.target as HTMLInputElement;
		const archivo: File = (target.files as FileList)[0];

		this.imagenSubir = archivo;
		if (!archivo) {
			console.log('sin carga archivo', this.imagenTemporal);
			return (this.imagenTemporal = null);
		}
		const reader = new FileReader();
		reader.readAsDataURL(archivo);
		reader.onload = () => {
			console.log('archivo', reader.result);
			this.imagenTemporal = reader.result as string;
		};

		return null;
	}

	subirImagen() {
		this.recomendacionService
			.actualizarFoto(this.imagenSubir, this.id)
			.then((resp: any) => {
				if (resp.operacion) {
					Swal.fire(
						'Actualización de la recomendación',
						// 'Imagen de médico actualizado correctamente',
						resp.mensaje,
						'success',
					);

					// this.imagenRecomendacion = `C:/allData/repos/REPO_DEEPSLEEP/backend-deepsleep/src/app/uploads/recomendaciones/${resp.nombreArchivo}.jpg`;
				} else {
					Swal.fire(
						'Actualización de la recomendación',
						// 'Imagen de médico actualizado correctamente',
						resp.mensaje,
						'error',
					);
				}
			})
			.catch((err) => {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: '¡Algo salió mal!',
				});
			});
	}

	irRecomendaciones() {
		this.router.navigate(['/recomendaciones']);
	}

	validarCreacionoUpdate() {
		if (this.id == '') {
			return 'https://us.123rf.com/450wm/urfandadashov/urfandadashov1809/urfandadashov180901275/109135379-photo-not-available-vector-icon-isolated-on-transparent-background-photo-not-available-logo-concept.jpg?ver=6';
		}
		return this.imagenDB;
	}
}

interface IHabitoSelect {
	_id: string;
	isSelected: boolean;
	nombre: string;
}