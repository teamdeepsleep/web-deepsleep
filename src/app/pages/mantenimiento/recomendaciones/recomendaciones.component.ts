import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IRecomendacion } from 'src/app/model/IRecomendacion';
import { RecomendacionService } from 'src/app/services/recomendacion.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-recomendaciones',
	templateUrl: './recomendaciones.component.html',
	styles: [],
})
export class RecomendacionesComponent implements OnInit {
	public cargando: boolean = true;
	public recomendaciones: IRecomendacion[] = [];
	base_url = environment.base_url;
	constructor(
		private router: Router,
		private recomendacionService: RecomendacionService,
	) {}

	ngOnInit(): void {
		this.cargarRecomendaciones();
	}

	cargarRecomendaciones() {
		this.cargando = true;
		this.recomendacionService.cargarRecomendaciones().subscribe((data) => {
			this.recomendaciones = data;
			console.log(data);
			this.cargando = false;
		});
	}

	buscar(termino: string) {}

	eliminarRecomendacion(id: any) {
		Swal.fire({
			title: '¿Eliminar Recomendación?',
			text: `Esta a punto de borrar la recoemndación`,
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si Borrarlo',
		}).then((result) => {
			if (result.value) {
				this.recomendacionService
					.eliminarRecomendacion(id)
					.subscribe((resp: any) => {
						console.log('pipipi', resp);
						if (resp.operacion === true) {
							this.cargarRecomendaciones();
							Swal.fire(
								'Recomendación eliminada',
								resp.mensaje,
								'success',
							);
						} else {
						}
					});
			}
		});
	}
	irCrearRecomendacion() {
		this.router.navigate(['/recomendacion-nuevo']);
	}

	irEditarRecomendacion(id: string) {
		this.router.navigate(['/recomendacion-editar/' + id]);
	}

	verImagen(recomendacion: IRecomendacion) {
		// var base_url = environment.base_url;
		return recomendacion.imagen;
		// return `${base_url}/upload/imagen/recomendaciones/${recomendacion.imagen}`;
	}
}
