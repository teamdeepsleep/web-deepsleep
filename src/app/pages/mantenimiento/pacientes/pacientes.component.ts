import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPaciente } from 'src/app/model/IPaciente';
import { PacienteService } from 'src/app/services/paciente.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-pacientes',
	templateUrl: './pacientes.component.html',
	styles: [],
})
export class PacientesComponent implements OnInit {
	public cargando: boolean = true;
	public pacientes: IPaciente[] = [];

	constructor(
		private router: Router,
		private pacienteService: PacienteService,
	) {}

	ngOnInit(): void {
		this.cargarPacientes();
	}

	cargarPacientes() {
		this.cargando = true;
		this.pacienteService.cargarPacientes().subscribe((data) => {
			this.pacientes = data;
			console.log(data);
			this.cargando = false;
		});
	}

	buscar(termino: string) {}

	habilitarEspecialista(id: string) {
		Swal.fire({
			title: 'Alerta',
			text: `¿Realmente desea activar al paciente elegido?`,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirmar',
			cancelButtonText: 'Cancelar',
			reverseButtons: true,
		}).then((result) => {
			console.log(result);
			if (result.isConfirmed) {
				this.pacienteService
					.cambiarEstadoPaciente(id, true)
					.subscribe((resp: any) => {
						this.cargarPacientes();

						if (resp.operacion) {
							Swal.fire(
								'Confirmación',
								// `${usuario.nombre} fue eliminado correctamente`,
								'se activó al paciente',
								'success',
							);
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'Ocurrio un error',
								// footer: `<a href>${error.message}</a>`
							});
						}
					});
			}
		});
	}

	deshabilitarEspecialista(id: string) {
		Swal.fire({
			title: 'Alerta',
			text: `¿Realmente desea desactivar al paciente elegido?`,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirmar',
			cancelButtonText: 'Cancelar',
			reverseButtons: true,
		}).then((result) => {
			console.log(result);
			if (result.isConfirmed) {
				this.pacienteService
					.cambiarEstadoPaciente(id, false)
					.subscribe((resp: any) => {
						this.cargarPacientes();

						if (resp.operacion) {
							Swal.fire(
								'Confirmación',
								// `${usuario.nombre} fue eliminado correctamente`,
								'se desactivó el paciente',
								'success',
							);
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'Ocurrio un error',
								// footer: `<a href>${error.message}</a>`
							});
						}
					});
			}
		});
	}

	verImagen(paciente: IPaciente) {
		// var base_url = environment.base_url;
		return paciente.imagen;
		// return `${base_url}/upload/imagen/pacientes/${paciente.imagen}`;
	}
}
