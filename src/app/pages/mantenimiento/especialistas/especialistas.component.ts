import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEspecialista } from 'src/app/model/IEspecialista';
import { EspecialistaService } from 'src/app/services/especialista.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-especialistas',
	templateUrl: './especialistas.component.html',
	styles: [],
})
export class EspecialistasComponent implements OnInit {
	public cargando: boolean = true;
	public especailistas: IEspecialista[] = [];

	constructor(
		private router: Router,
		private especialistaService: EspecialistaService,
	) {}

	ngOnInit(): void {
		this.cargarEspecialistas();
	}

	cargarEspecialistas() {
		this.cargando = true;
		this.especialistaService.cargarEspecialistas().subscribe((data) => {
			this.especailistas = data;
			console.log(data);
			this.cargando = false;
		});
	}

	buscar(termino: string) {}
	habilitarEspecialista(id: string) {
		Swal.fire({
			title: 'Alerta',
			text: `¿Realmente desea activar al especialista elegido?`,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirmar',
			cancelButtonText: 'Cancelar',
			reverseButtons: true,
		}).then((result) => {
			console.log(result);
			if (result.isConfirmed) {
				this.especialistaService
					.cambiarEstadoEspecialista(id, true)
					.subscribe((resp: any) => {
						this.cargarEspecialistas();

						if (resp.operacion) {
							Swal.fire(
								'Confirmación',
								// `${usuario.nombre} fue eliminado correctamente`,
								'se activó al especialista',
								'success',
							);
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'Ocurrio un error',
								// footer: `<a href>${error.message}</a>`
							});
						}
					});
			}
		});
	}
	deshabilitarEspecialista(id: string) {
		Swal.fire({
			title: 'Alerta',
			text: `¿Realmente desea desactivar al especialista elegido?`,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirmar',
			cancelButtonText: 'Cancelar',
			reverseButtons: true,
		}).then((result) => {
			console.log(result);
			if (result.isConfirmed) {
				this.especialistaService
					.cambiarEstadoEspecialista(id, false)
					.subscribe((resp: any) => {
						this.cargarEspecialistas();

						if (resp.operacion) {
							Swal.fire(
								'Confirmación',
								// `${usuario.nombre} fue eliminado correctamente`,
								'se desactivó el especialista',
								'success',
							);
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'Ocurrio un error',
								// footer: `<a href>${error.message}</a>`
							});
						}
					});
			}
		});
	}

	verImagen(especialista: IEspecialista) {
		// var base_url = environment.base_url;
		return especialista.imagen;
		// return `${base_url}/upload/imagen/recomendaciones/${especialista.imagen}`;
	}
}
