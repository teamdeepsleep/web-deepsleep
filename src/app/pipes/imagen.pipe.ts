import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

const url = environment.base_url;
@Pipe({
	name: 'imagen',
})
export class ImagenPipe implements PipeTransform {
	transform(
		imagen: any,
		tipo: 'paciente' | 'especialista' | 'habito' | 'recomendacion',
	): string {
		console.log('imagen' + imagen);
		if (!imagen) {
			return `https://us.123rf.com/450wm/urfandadashov/urfandadashov1809/urfandadashov180901275/109135379-photo-not-available-vector-icon-isolated-on-transparent-background-photo-not-available-logo-concept.jpg?ver=6`;
		}
		// if (!imagen) {
		// 	return `${url}/upload/${tipo}/no-image`;
		// } else if (imagen) {
		// 	return `${url}/upload/${tipo}/${imagen}`;
		// } else {
		// 	return `${url}/upload/${tipo}/no-image`;
		// }

		return 'hola mundo ';
	}
}
